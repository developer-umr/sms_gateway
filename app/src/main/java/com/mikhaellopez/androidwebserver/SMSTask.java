package com.mikhaellopez.androidwebserver;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by altab on 11/1/2017.
 */

public class SMSTask extends AsyncTask {
    public static final String TAG="sms_gateway";
    private Activity activity;
    private JSONObject clientes;
    private int max;
    private String text;
    public int timeout;

    public SMSTask(Activity a, JSONObject c) {
        activity = a;
        clientes = c;
        timeout = 1000;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        System.out.println("Trabajando en segundo plano!");
        System.out.println("________________________________");
        System.out.println(clientes.toString());
        System.out.println("________________________________");
        try {
            JSONObject cliente = clientes;
            System.out.println("tp" + cliente.getString("phone"));
            sendMsg(cliente.getString("phone"), cliente.getString("sms"));
            //Thread.sleep(timeout);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Log.d(TAG, "onPostExecute: Termino enviar o no un mensaje");
    }

    public void sendMsg(final String phoneNumber, String msgText) {
        try {
            PendingIntent sentPI = PendingIntent.getBroadcast(this.activity, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this.activity, 0, new Intent("SMS_DELIVERED"), 0);

            activity.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(activity.getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                            System.out.println("SMS delivered");
                            break;
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(activity.getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                            System.out.println("SMS not delivered");
                            break;
                    }
                }
            }, new IntentFilter("SMS_DELIVERED"));

            activity.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(activity.getBaseContext(), "SMS sent " + phoneNumber, Toast.LENGTH_SHORT).show();
                            System.out.println("SMS sent " + phoneNumber);
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            Toast.makeText(activity.getBaseContext(), "Generic failure " + phoneNumber, Toast.LENGTH_SHORT).show();
                            System.out.println("Generic failure " + phoneNumber);
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            Toast.makeText(activity.getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                            System.out.println("No service");
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            Toast.makeText(activity.getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                            System.out.println("Null PDU");
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            Toast.makeText(activity.getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                            System.out.println("Radio off");
                            break;
                        default:
                            break;
                    }
                }
            }, new IntentFilter("SMS_SENT"));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, msgText, sentPI, deliveredPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
