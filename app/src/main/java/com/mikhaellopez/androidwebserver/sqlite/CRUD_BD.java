package com.mikhaellopez.androidwebserver.sqlite;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import intranet.client.reloj_checador.modelos.Empleado;
import intranet.client.reloj_checador.modelos.Empresa;
import intranet.client.reloj_checador.modelos.Geocerca;
import intranet.client.reloj_checador.modelos.Horario;
import intranet.client.reloj_checador.modelos.TimeLog;

/***
 * Clase: CRUD BD
 * Contiene metodos staticos con las operaciones CRUD.
 */

public class CRUD_BD {
    private static final String TAG="reloj";
    //CREATE
    public static void insertarMensaje(Mensaje mensaje, AyudanteSqlite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("telofono", mensaje.getTelefono());
        cv.put("mensaje", mensaje.getMensaje());
        sqLiteDatabase.insert("empresas", "id_empresa", cv);
        sqLiteDatabase.close();
    }

    public static ArrayList<Mensaje> obtenerMensaje(AyudanteSqlite ayudanteSqlite){
        ArrayList<Mensaje> mensajes = new ArrayList<Mensaje>();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","DT_RowId","id_geocerca","descripcion","latitud","longitud","radio","wifimac","activo"};
        try{
            Cursor c=sqLiteDatabase.query("geocercas",columnas,null,null,null,null,null);
            while(c.moveToNext()){
                Mensaje mensaje=new Mensaje();
                mensaje.setTelefono((long)c.getInt(2));
                geocerca.setDT_RowId((long)c.getInt(1));
                geocerca.setDescripcion(c.getString(3));
                geocerca.setLatitud(c.getString(4));
                geocerca.setLongitud(c.getString(5));
                geocerca.setRadio(c.getString(6));
                geocerca.setMacsE(c.getString(7));
                if(c.getInt(8)==1){
                    geocerca.setActivo(true);
                } else{
                    geocerca.setActivo(false);
                }
                geocercas.add(geocerca);
            }
        }catch (Exception e){
            geocercas=null;
        }
        sqLiteDatabase.close();
        return geocercas;
    }

    public static Empresa obtenerEmpresa(AyudanteSqlite ayudanteSqlite){
        Empresa empresa=new Empresa();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","id_empresa","clave","razon_social","rfc"};
        Cursor c=sqLiteDatabase.query("empresas",columnas,null,null,null,null,null);
        if(c.moveToFirst()){
            empresa.setId((long)c.getInt(1));
            empresa.setClave(c.getString(2));
            empresa.setRazonSocial(c.getString(3));
            empresa.setRfc(c.getString(4));
        }
        sqLiteDatabase.close();
        return empresa;
    }
    public static ArrayList<Empresa> obtenerEmpresas(AyudanteSqlite ayudanteSqlite){
        ArrayList<Empresa> empresas=new ArrayList<Empresa>();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","id_empresa","clave","razon_social","rfc","clave_empresa","actividad","activo"};
        Cursor c=sqLiteDatabase.query("empresas",columnas,null,null,null,null,null);
        while (c.moveToNext()){
            Empresa empresa=new Empresa();
            empresa.setId((long)c.getInt(1));
            empresa.setClave(c.getString(2));
            empresa.setRazonSocial(c.getString(3));
            empresa.setRfc(c.getString(4));
            empresa.setClaveEmpresa(c.getString(5));
            empresa.setActividad(c.getString(6));
            if(c.getInt(7)==1){
                empresa.setActivo(true);
            } else{
                empresa.setActivo(false);
            }
            empresas.add(empresa);
        }
        sqLiteDatabase.close();
        return empresas;
    }
    public static ArrayList<Empleado> obtenerEmpleados(AyudanteSqlite ayudanteSqlite){
        ArrayList<Empleado> empleados=new ArrayList<Empleado>();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","DT_RowId","id_empleado","clave","nombre","fk_id_empresa","uuid","fk_id_horario"};
        Cursor c=sqLiteDatabase.query("empleados",columnas,null, null, null, null, null);
        while (c.moveToNext()){
            Empleado empleado=new Empleado();
            empleado.setDT_RowId((long)c.getInt(1));
            empleado.setId((long)c.getInt(2));
            empleado.setClave(c.getString(3));
            empleado.setNombre(c.getString(4));
            empleado.setUuid(c.getString(6));
            empleados.add(empleado);
        }
        sqLiteDatabase.close();
        return empleados;
    }

    public static ArrayList<TimeLog> obtenerLogs(AyudanteSqlite ayudanteSqlite, String empleado_id){
        ArrayList<TimeLog> timeLogs=new ArrayList<TimeLog>();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","empleado_id","geocerca_id","imagen","latitud","longitud","registro","activo","tipo"};
        Cursor c;
        if(empleado_id.equals("no")) {
            c = sqLiteDatabase.query("sistema_logs", columnas, null, null, null, null, "id ASC");
        } else{
            c = sqLiteDatabase.query("sistema_logs", columnas, "empleado_id=?", new String[]{empleado_id}, null, null, "id ASC");
        }
        while(c.moveToNext()){
            TimeLog timeLog=new TimeLog();
            timeLog.setId((long)c.getInt(0));
            timeLog.setUuid(c.getString(1));
            timeLog.setGeocerca((long)c.getInt(2));
            timeLog.setImg(c.getString(3));
            timeLog.setLatitud(c.getString(4));
            timeLog.setLongitud(c.getString(5));
            timeLog.setRegistro(c.getString(6));
            if(c.getInt(7)==1){
               timeLog.setStatus(true);
            } else{
                timeLog.setStatus(false);
            }
            timeLog.setTipo(c.getInt(8));
            timeLogs.add(timeLog);
        }
        sqLiteDatabase.close();
        return timeLogs;
    }

    public static TimeLog obtenerLogPorID(AyudanteSqlite ayudanteSqlite, String id_empleado){
        TimeLog timeLog=new TimeLog();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","empleado_id","geocerca_id","imagen","latitud","longitud","registro","activo","tipo"};
        Cursor c=sqLiteDatabase.query("sistema_logs",columnas,"empleado_id=?",new String[]{id_empleado},null,null,"id DESC");
        if(c.moveToFirst()){
            timeLog.setId(Long.parseLong(c.getString(1)));
            timeLog.setUuid(c.getString(1));
            timeLog.setGeocerca((long)c.getInt(2));
            timeLog.setImg(c.getString(3));
            timeLog.setLatitud(c.getString(4));
            timeLog.setLongitud(c.getString(5));
            timeLog.setRegistro(c.getString(6));
            if(c.getInt(7)==1){
                timeLog.setStatus(true);
            } else{
                timeLog.setStatus(false);
            }
            timeLog.setTipo(c.getInt(8));
        } else {
            timeLog=null;
        }
        sqLiteDatabase.close();
        return timeLog;
    }

    public static Empleado obtenerEmpleadoPorClave(AyudanteSqlite ayudanteSqlite, String clave){
        Empleado empleado=new Empleado();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","DT_RowId","id_empleado","clave","nombre","fk_id_empresa","uuid","fk_id_horario"};
        Cursor c=sqLiteDatabase.query("empleados",columnas,"clave=?", new String[]{clave}, null,null,null);
        if(c.moveToFirst()){
            empleado.setId((long)c.getInt(2));
            empleado.setNombre(c.getString(4));
            //En la nomina recibe como identificador de empleado uuid (pero este puede ser su id o su uuid)
            if(c.getString(6)!=null){
                empleado.setUuid(c.getString(6));
            } else{
                empleado.setUuid(""+c.getInt(2));
            }
            empleado.setHorario_id(c.getString(7));
        } else{
            empleado=null;
        }
        sqLiteDatabase.close();
        return empleado;
    }

    public static Horario obtenerHorarioPorID(AyudanteSqlite ayudanteSqlite, String id_horario){
        Horario horario=new Horario();
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"id","id_horario","descripcion","entrada","maxSalida","seguridad"};
        Cursor c=sqLiteDatabase.query("horarios",columnas,"id_horario=?", new String[]{id_horario},null,null,null);
        if(c.moveToFirst()){
            horario.setId((long)c.getInt(1));
            horario.setDescripcion(c.getString(2));
            horario.setEntrada(c.getString(3));
            horario.setMaxSalida(c.getString(4));
            horario.setG_seguridad(c.getString(5));
        }else{
            horario=null;
        }
        return horario;
    }

    public static long obtenerID(String tabla, String columnaClave, String valorColumnaClave, AyudanteSqlite ayudanteSQLite) {
        long id=0;
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getReadableDatabase();
        Cursor c = sqLiteDatabase.query(tabla, new String[]{columnaClave}, columnaClave + "=?", new String[]{valorColumnaClave},
                null, null, null);
        if(c.moveToFirst()){
            id=(long)c.getColumnIndex(columnaClave);
        }else{
            //comunicar a nivel de usuario comun
        }
        return id;
    }

    public static int obtenerStatusLog(AyudanteSqlite ayudanteSqlite){
        int activo=0;
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getReadableDatabase();
        String[] columnas={"activo"};
        Cursor c=sqLiteDatabase.rawQuery("SELECT activo FROM sistema_logs WHERE id=(SELECT MAX(id) FROM sistema_logs)",new String[]{});
        if(c.moveToFirst()){
            activo=c.getInt(0);
        }
        return activo;
    }
    //UPDATE
    //DELETE
    public static void eliminarGeocercas(AyudanteSqlite ayudanteSqlite){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("geocercas", null, null);
        sqLiteDatabase.close();
    }
    public static void eliminarEmpresas(AyudanteSqlite ayudanteSqlite){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("empresas", null, null);
        sqLiteDatabase.close();
    }
    public static void eliminarEmpleados(AyudanteSqlite ayudanteSqlite){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("empleados", null, null);
        sqLiteDatabase.close();
    }

    public static void eliminarLogs(AyudanteSqlite ayudanteSqlite){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("sistema_logs",null,null);
        sqLiteDatabase.close();
    }

    public static void eliminarLogsPorId(AyudanteSqlite ayudanteSqlite, String empleado_id){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("sistema_logs","empleado_id=?", new String[]{empleado_id});
        sqLiteDatabase.close();
    }

    public static void eliminarHorarios(AyudanteSqlite ayudanteSqlite){
        SQLiteDatabase sqLiteDatabase=ayudanteSqlite.getWritableDatabase();
        sqLiteDatabase.delete("horarios",null,null);
        sqLiteDatabase.close();
    }

    //modificar metodo ya que si elimina por id_empleado podria eliminar registros que no se han enviado a la nomina
    public static void eliminarLog(String id, AyudanteSqlite ayudanteSQLite) {
        SQLiteDatabase sqLiteDatabase = ayudanteSQLite.getWritableDatabase();
        sqLiteDatabase.delete("sistema_logs", "id=?", new String[]{String.valueOf(id)});
        sqLiteDatabase.close();
    }
}
