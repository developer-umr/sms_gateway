package com.mikhaellopez.androidwebserver.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase: Ayudante Sqlite
 * Contiene toda la estructura de la base de datos
 */

public class AyudanteSqlite extends SQLiteOpenHelper{
    private static final String NOMBRE_BD="sms_masivo";
    private static final String TABLA_EMPRESA="" +
            "CREATE TABLE " +
            "mensajes(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "telefono TEXT," +
            "mensaje TEXT" +
            ")";

    public AyudanteSqlite(Context context) {
        super(context, NOMBRE_BD, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLA_EMPRESA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS mensajes");
    }
}
