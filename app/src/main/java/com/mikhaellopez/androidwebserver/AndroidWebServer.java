package com.mikhaellopez.androidwebserver;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.StringTokenizer;

import fi.iki.elonen.NanoHTTPD;

/**
 * Created by altab on 11/1/2017.
 */

public class AndroidWebServer extends NanoHTTPD {
    private Activity activity;
    public static final String TAG = "sms_gateway";

    public AndroidWebServer(int port) {
        super(port);
    }

    public AndroidWebServer(int port, Activity activity) {
        super(port);
        this.activity = activity;
    }

    public AndroidWebServer(String hostname, int port) {
        super(hostname, port);
    }

    @Override
    public Response serve(IHTTPSession session) {
        Map<String, String> params = session.getParms();
        String response = "{\"hello\":\"hello world\",\"success\":\"true\"}";
        String data = (params.get("work"));
        String[] tokens = data.split(",");
        Log.d(TAG, "serve: data: " + data);
        for (int i = 0; i < tokens.length; i++) {
            Log.d(TAG, "serve: token: " + i + ": " + tokens[i]);
            String[] phoneNumberTok = tokens[i].split(":");

            JSONObject workToTask = new JSONObject();
            Log.d(TAG, "serve: " + data);
            String phone = "";
            String sms = "", sms2 = "";

            phone = phoneNumberTok[0];
            sms = phoneNumberTok[1];

            try {
                workToTask.put("phone", phone);
                workToTask.put("sms", sms.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            SMSTask tarea = new SMSTask(activity, workToTask);
            tarea.execute();
            //sms2 = phoneNumberTok[2] + phoneNumberTok[3];
            Log.d(TAG, "serve: phone: " + phone);
            Log.d(TAG, "serve: sms: " + sms);
            Log.d(TAG, "serve: sms2: " + sms2);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            /*try {
                JSONObject workToTask = new JSONObject();
                SMSTask tarea = new SMSTask(activity, workToTask);
                for (int h = 0; h < 2; h++) {
                    workToTask.put("phone", phone);
                    if (h == 0)
                        workToTask.put("sms", sms.toString());
                    else
                        workToTask.put("sms", sms2.toString());
                    tarea.execute();
                    workToTask=new JSONObject();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
        //sendMsg(phone,sms);
        //System.out.println(workToTask.toString());

        return newFixedLengthResponse(status(), "application/json", response);
    }

    private JSONArray fetchWork(String data) {
        return null;
    }

    private Response.IStatus status() {
        Response.IStatus status = new Response.IStatus() {
            @Override
            public String getDescription() {
                return "It's ok.";
            }

            @Override
            public int getRequestStatus() {
                return 207;
            }
        };
        return status;
    }

    public void sendMsg(final String phoneNumber, String msgText) {
        try {
            PendingIntent sentPI = PendingIntent.getBroadcast(this.activity, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredPI = PendingIntent.getBroadcast(this.activity, 0, new Intent("SMS_DELIVERED"), 0);

            activity.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(activity.getBaseContext(), "SMS sent " + phoneNumber, Toast.LENGTH_SHORT).show();
                            System.out.println("SMS sent " + phoneNumber);
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            Toast.makeText(activity.getBaseContext(), "Generic failure " + phoneNumber, Toast.LENGTH_SHORT).show();
                            System.out.println("Generic failure " + phoneNumber);
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            Toast.makeText(activity.getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                            System.out.println("No service");
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            Toast.makeText(activity.getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                            System.out.println("Null PDU");
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            Toast.makeText(activity.getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                            System.out.println("Radio off");
                            break;
                        default:
                            break;
                    }
                }
            }, new IntentFilter("SMS_SENT"));
            activity.registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(activity.getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                            System.out.println("SMS delivered");
                            break;
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(activity.getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                            System.out.println("SMS not delivered");
                            break;
                    }
                }
            }, new IntentFilter("SMS_DELIVERED"));

            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phoneNumber, null, msgText, sentPI, deliveredPI);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
